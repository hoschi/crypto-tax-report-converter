#!/usr/bin/env perl
# convert bsdex.de tax export to accointing.com wallet import format

# stdin:  bsdex.de csv content
# stdout: accointing.com csv

use strict;
use warnings;

my %typeConvert = (
	buy => "order",
	sell => "order",
	withdrawal => "withdraw",
	deposit => "deposit",
);

sub buildCsvRow {
	my @cells = @_;
	return '"' . join('","', @cells) . '"' . "\n";
}

print buildCsvRow(
	"transactionType",
	"date",
	"inBuyAmount",
	"inBuyAsset",
	"outSellAmount",
	"outSellAsset",
	"feeAmount (optional)",
	"feeAsset (optional)",
	"classification (optional)",
	"operationId (optional)",
	"comments (optional)",
);

# skip first row (headings)
<STDIN>;

while (<STDIN>) {
    my @bsdexFields = split ",";
    @bsdexFields = map { $_ eq '""' ? "" : $_} @bsdexFields;
    # amt := amount
    # ass := asset
    my (
		$id,
		$type,
		$date,
		$inAmt,
		$inAss,
		$outAmt,
		$outAss,
		$feeAmt,
		$feeAss,
	) =  @bsdexFields;
	$type = $typeConvert{$type};
	$date =~ s|^(\d{4})-(\d{2})-(\d{2})|$2/$3/$1|;
	$date =~ s/ \+0000$//;

	print buildCsvRow(
		$type,
		$date,
		$inAmt,
		$inAss,
		$outAmt,
		$outAss,
		$feeAmt,
		$feeAss,
		"", # classification
		$id,
	);
}
